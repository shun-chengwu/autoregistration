#define USE_SHOT
#define USE_VIEWER

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>

#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/icp.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/pfh.h>
#include <pcl/features/shot.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/random_sample.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/filters/statistical_outlier_removal.h>

#ifdef USE_VIEWER
#include <pcl/visualization/pcl_visualizer.h>
#endif

#include <pcl/keypoints/harris_3d.h>
#include <pcl/keypoints/iss_3d.h>

#include <pcl/common/io.h>
#include <pcl/common/common.h>
#include <pcl/common/angles.h>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "src/Header.h"
#include "src/config.h"

//#include "../libicp/matrix.h"
//#include "../libicp/icp.h"
//#include "../libicp/icpPointToPoint.h"
//#include "../libicp/icpPointToPlane.h"
//#include "../libicp/kdtree.h"

/*Main function*/
int main(int argc, char *argv[]) {
#ifdef USE_VIEWER
    pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("debug viewer"));
    int vp0, vp1;
    viewer->createViewPort(0, 0, 0.5, 1, vp0);
    viewer->createViewPort(0.5, 0, 1, 1, vp1);
#endif
    
    
    /*Setting up all we need*/
    float maxRad;
    Parameters params;
    float radiusSearchSource, radiusSearchTarget;
    PCPointTPtr mSourceCloud(new PCPointT); // used in processing
    PCPointTPtr mTargetCloud(new PCPointT); // used in processing
    PCPointTPtr mSourceKeypoints(new PCPointT);
    PCPointTPtr mTargetKeypoints(new PCPointT);
    PCPointTPtr mSourceTransformedCloud(new PCPointT);
    PCPointTPtr mSourceRegisteredCloud(new PCPointT);
    PCPointTPtr mSourceCloudCopy(new PCPointT); // the original one
    PCPointTPtr mTargetCloudCopy(new PCPointT); // the original one
    PCPointNPtr mSourceNormals(new PCPointN);
    PCPointNPtr mTargetNormals(new PCPointN);
    pcl::CorrespondencesPtr mCorrespondences_Source2target(new pcl::Correspondences);
    pcl::CorrespondencesPtr mCorrespondences_Target2source(new pcl::Correspondences);
    pcl::CorrespondencesPtr mFinalCorrespondences(new pcl::Correspondences);
    Eigen::Matrix4f mInitialTransformationMatrix;
    pcl::NormalEstimation<PointT, PointN> ne;
    pcl::NormalEstimation<PointT, PointN> ne2;
#ifdef USE_SHOT
    typedef pcl::SHOT352 DescriptorType;
#else
    typedef pcl::PFHSignature125 DescriptorType;
#endif
    pcl::PointCloud<DescriptorType>::Ptr mPFHSourceDescriptors(new pcl::PointCloud<DescriptorType>);
    pcl::PointCloud<DescriptorType>::Ptr mPFHTargetDescriptors(new pcl::PointCloud<DescriptorType>);

    /* Next variables are  the Paths and names of files with the input/output data **
    ** We ask user to introduces the files and the pathe to place the results      */
    std::string rutaSourceCloud;
    std::string rutaTargetCloud;
    std::string rutaSalida;

    std::cout << "Path to the source Pointcloud: " << std::endl;
    rutaSourceCloud = argv[1];
    printf(" %s\n", rutaSourceCloud.c_str());

    std::cout << "Path to the target Pointcloud: " << std::endl;
    rutaTargetCloud = argv[2];
    printf(" %s\n", rutaTargetCloud.c_str());

    std::cout << "Path to save resultant file: " << std::endl;
    rutaSalida = argv[3];
    printf(" %s\n", rutaSalida.c_str());

    /*Load data from both files*/
    {
        printf("---\n");
        printf("-Load Data-\n");
        pcl::io::loadPLYFile (rutaSourceCloud, *mSourceCloud);
        pcl::io::loadPLYFile (rutaTargetCloud, *mTargetCloud);
        
        //xyzrgbloader(rutaSourceCloud, mSourceCloud, '\t');
        //xyzrgbloader(rutaTargetCloud, mTargetCloud, '\t');
        pcl::copyPointCloud(*mSourceCloud, *mSourceCloudCopy);
        pcl::copyPointCloud(*mTargetCloud, *mTargetCloudCopy);
        std::cout << " Step 0 of 11: Source cloud and target cloud loaded succesfully\n" << std::endl;
        printf("-Done!.\n");
        printf("---\n");
    }
    
    /* Downsampling */
    {
        /*Calculate average resolution of clouds*/
        {
            /*Before compute normals, we calculate the average minimum distance between points
             in order to establish a radius search according to the data each time*/
            printf("---\n");
            printf("-Calculate average resolution of clouds-\n");
            calcularDistanciaMinimaMedia(mSourceCloud, radiusSearchSource);
            calcularDistanciaMinimaMedia(mTargetCloud, radiusSearchTarget);
            maxRad = std::max(radiusSearchSource, radiusSearchTarget);
            printf("-Done!.\n");
            printf("---\n");
        }
        
        printf("---\n");
        printf("-downsampling for debug-\n");
        auto VoxelGridDownsampling = [](PCPointTPtr cloud_filtered, PCPointTPtr cloud, std::vector<float> gridsize){
            if(gridsize.size()==1){
                gridsize.push_back(gridsize[0]);
                gridsize.push_back(gridsize[0]);
            }
            pcl::VoxelGrid<PointT> vg;
            vg.setInputCloud (cloud);
            vg.setLeafSize (gridsize[0], gridsize[1], gridsize[2]);
            vg.filter (*cloud_filtered);
        };
        VoxelGridDownsampling(mSourceCloud, mSourceCloud, std::vector<float>{params.downsampleRad*maxRad});
        VoxelGridDownsampling(mTargetCloud, mTargetCloud, std::vector<float>{params.downsampleRad*maxRad});
        printf("-Done!.\n");
        printf("---\n");
        // calculate resolution again because of downsampling
        {
            printf("---\n");
            printf("-Calculate average resolution of clouds-\n");
            calcularDistanciaMinimaMedia(mSourceCloud, radiusSearchSource);
            calcularDistanciaMinimaMedia(mTargetCloud, radiusSearchTarget);
            maxRad = std::max(radiusSearchSource, radiusSearchTarget);
            printf("-Done!.\n");
            printf("---\n");
        }
    }
    
    /* Remove outliers */
    {
        printf("---\n");
        printf("-StatisticalOutlierRemoval-\n");
        auto filteringobject = [](PCPointTPtr cloud,int meank, int stddevmulthresh) ->PCPointTPtr{
            PCPointTPtr cloud_filtered (new pcl::PointCloud<PointT>);;
            pcl::StatisticalOutlierRemoval<PointT> sor;
            sor.setInputCloud (cloud);
            sor.setMeanK (meank);
            sor.setStddevMulThresh (stddevmulthresh);
            sor.filter (*cloud_filtered);
            return cloud_filtered;
        };
        auto Sourcefiltered = filteringobject
        (mSourceCloud,
         params.StatisticalOutlierRemoval.meank,
         params.StatisticalOutlierRemoval.std);
        auto Targetfiltered = filteringobject
        (mTargetCloud,
         params.StatisticalOutlierRemoval.meank,
         params.StatisticalOutlierRemoval.std);
        
#ifdef USE_VIEWER
        viewer->removeAllPointClouds();
        printf(" Display filtered SourceCloud..\n");
        viewer->addPointCloud(mSourceCloud, "mSourceCloud", vp0);
        viewer->addPointCloud(Sourcefiltered, "Sourcefiltered", vp1);
        viewer->spin();
        viewer->removeAllPointClouds();
        printf(" Display filtered TargetCloud..\n");
        viewer->addPointCloud(mTargetCloud, "mTargetCloud", vp0);
        viewer->addPointCloud(Targetfiltered, "Targetfiltered", vp1);
        viewer->spin();
        viewer->removeAllPointClouds();
#endif
        
        mSourceCloud = Sourcefiltered;
        mTargetCloud = Targetfiltered;
        printf("-Done!.\n");
        printf("---\n");
    }
    
    
    /*Compute normals*/
    {
        printf("---\n");
        printf("-Compute Normal-\n");
        /*We comupte the pointcloud centroid to orient correctl normals*/
        Eigen::Vector4f centroid_src, centroid_trg;
        {
            printf(" Calculate Centroids...");
            compute3DCentroid(*mSourceCloud, centroid_src);
            compute3DCentroid(*mTargetCloud, centroid_trg);
            printf(" Done!.\n");
        }
        
        auto computeNormal=[](PCPointNPtr normal, PCPointTPtr source, Eigen::Vector4f centroid, float radius){
            pcl::NormalEstimation<PointT, PointN> ne;
            pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
            ne.setInputCloud(source);
            ne.setViewPoint(centroid[0], centroid[1], centroid[2]);
            ne.setSearchMethod(tree);
            ne.setKSearch(5);
            //        ne.setRadiusSearch(10*radius);//FIXME:using radiusSearchSource is a bit too small. Some normals are calculated with only few points.
            ne.compute(*normal);
        };
        computeNormal(mSourceNormals, mSourceCloud, centroid_src, radiusSearchSource);
        computeNormal(mTargetNormals, mTargetCloud, centroid_trg, radiusSearchTarget);
        
        std::cout << "  Before Normals sanity check: " << mSourceNormals->size() << " normals computed" << std::endl;
        removeNANelements(mSourceCloud, mSourceNormals);
        std::cout << "  Step 1 of 11:->Normlas on source cloud estimated succesfully (" << mSourceNormals->size() << ")" << std::endl;
        
        std::cout << "  Before sanity check: " << mTargetNormals->size() << " normals computed" << std::endl;
        removeNANelements(mTargetCloud, mTargetNormals);
        std::cout << "  Step 2 of 11:->Normlas on target cloud estimated succesfully (" << mTargetNormals->size() << ")" << std::endl;
        
#ifdef USE_VIEWER
        printf("-Display Centroid and normal...\n");
        viewer->addPointCloud(mSourceCloud, "cloud_src",vp0);
        viewer->addPointCloud(mTargetCloud, "cloud_trg",vp1);
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, "cloud_src");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 0, 1, "cloud_trg");
        /* SHOW centroid */
        printf(" Centroids\n");
        viewer->addSphere(PointXYZ(centroid_src[0], centroid_src[1], centroid_src[2]), radiusSearchSource, 1, 0, 0, "centroidSrc", vp0);
        viewer->addSphere(PointXYZ(centroid_trg[0], centroid_trg[1], centroid_trg[2]), radiusSearchTarget, 1, 0, 0, "centroidTrg", vp1);
        /* SHOW normal */
        printf(" Normals\n");
        viewer->addPointCloudNormals<PointT, PointN>(mSourceCloud, mSourceNormals, 1, 0.05, "normal_src",vp0);
        viewer->addPointCloudNormals<PointT, PointN>(mTargetCloud, mTargetNormals, 1, 0.05, "normal_trg",vp1);
        viewer->spin();
        viewer->removeShape("centroidSrc");
        viewer->removeShape("centroidTrg");
        viewer->removePointCloud("normal_src");
        viewer->removePointCloud("normal_trg");
#endif
        
        printf("-Done!.\n");
        printf("---\n");
    }
    
    
    
    /*Now Harris3D detector is computed for both pointclouds*/
    {
        printf("-Compute Keypoints-\n");
        auto compute_keypoints = [] (PCPointTPtr pc_ori, float rad, PCPointTPtr pc_key){
            pcl::HarrisKeypoint3D<PointT, PointTI> harris3D;
            harris3D.setInputCloud (pc_ori);
            harris3D.setNonMaxSupression (true);
            harris3D.setRadiusSearch (rad);
            harris3D.setRadius(rad);
            harris3D.setMethod(pcl::HarrisKeypoint3D<PointT,PointTI, PointN>::NOBLE);
            harris3D.setRefine(false);
            harris3D.setThreshold (0.01f);
            harris3D.setMethod(pcl::HarrisKeypoint3D<PointT,PointTI>::HARRIS);
            pcl::PointCloud<PointTI>::Ptr tmpKeypoints (new pcl::PointCloud<PointTI>());
            harris3D.compute(*tmpKeypoints);
            getKeypointsCloud(pc_ori, tmpKeypoints, pc_key);
        };
        auto compute_ISS_keypoints = [&] (PCPointTPtr pc_ori, float rad, PCPointTPtr pc_key) {
            params.initISS(rad);
            
            pcl::ISSKeypoint3D<PointT, PointT> iss_detector;
            iss_detector.setSalientRadius (params.ISS.salientRad);
            iss_detector.setNonMaxRadius (params.ISS.nonMaxRad);
            iss_detector.setNormalRadius (params.ISS.normalRad);
            iss_detector.setBorderRadius (params.ISS.borderRad);
            iss_detector.setThreshold21 (params.ISS.thres21);
            iss_detector.setThreshold32 (params.ISS.thres32);
            iss_detector.setMinNeighbors (params.ISS.minNeighbors);
            iss_detector.setNumberOfThreads (params.ISS.numThread);
            iss_detector.setInputCloud (pc_ori);
            iss_detector.compute (*pc_key);
        };
        compute_ISS_keypoints(mSourceCloud, maxRad, mSourceKeypoints);
        compute_ISS_keypoints(mTargetCloud, maxRad, mTargetKeypoints);
        std::cout << " Step 3 of 11:->Source:-> " << mSourceKeypoints->size() << " Keypoints detected\n" << std::endl;
        std::cout << " Step 4 of 11:->Target:-> " << mTargetKeypoints->size() << " Keypoints detected\n" << std::endl;
        printf("-Done!.\n");
        printf("---\n");
    }
    
    /* Show point cloud */
    {
#ifdef USE_VIEWER
        //Show keypoint
        viewer->removeAllPointClouds();
        viewer->addPointCloud(mSourceCloud, "cloud_src",vp0);
        viewer->addPointCloud(mTargetCloud, "cloud_trg",vp1);
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, "cloud_src");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 0, 1, "cloud_trg");
        viewer->addPointCloud(mSourceKeypoints, "mSrcKey",vp0);
        viewer->addPointCloud(mTargetKeypoints, "mTarKey",vp1);
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 1, 0, 0, "mSrcKey");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 1, 0, 0, "mTarKey");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "mSrcKey");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "mTarKey");
        printf(" Show Keypoints\n");
        viewer->spin();
        viewer->removePointCloud("mSrcKey");
        viewer->removePointCloud("mTarKey");
#endif
        printf("-Done!.\n");
        printf("---\n");
    }
    
    /*With detected points computed, we go over the descriptor method, using PFH */
    {
        printf("-Compute Descriptors-\n");
        params.initDesp(maxRad);
#ifdef USE_SHOT
        auto compute_descriptor = [&](PCPointTPtr pc, PCPointTPtr pc_key, PCPointNPtr pc_n, float rad, PCPtr(pcl::SHOT352) descriptors){
            pcl::SHOTEstimation<PointT, PointN> shot;
            shot.setInputCloud(pc_key);
            shot.setInputNormals(pc_n);
            shot.setSearchSurface(pc);
            shot.setRadiusSearch(rad);
            shot.compute(*descriptors);
        };
#else
        auto compute_descriptor = [&](PCPointTPtr pc, PCPointTPtr pc_key, PCPointNPtr pc_n, float rad, PCPtr(pcl::PFHSignature125) descriptors){
            pcl::PFHEstimation<PointT, PointN, DescriptorType> pfh;
            pfh.setInputCloud (pc_key);
            pfh.setInputNormals (pc_n);
            pfh.setSearchSurface(pc);
            pfh.setRadiusSearch(rad);
            pfh.compute (*descriptors);
        };
#endif
        compute_descriptor(mSourceCloud, mSourceKeypoints, mSourceNormals, params.Desp.rad, mPFHSourceDescriptors);
        compute_descriptor(mTargetCloud, mTargetKeypoints, mTargetNormals, params.Desp.rad, mPFHTargetDescriptors);
        std::cout << " Step 5 of 11:->Source Descriptor computed successfully " << mPFHSourceDescriptors->size() << " descriptors" << std::endl;
        std::cout << " Step 6 of 11:->Target Descriptor computed successfully " << mPFHTargetDescriptors->size() << " descriptors" << std::endl << std::endl;
        printf("-Done!.\n");
        printf("---\n");
    }

    /* Matching from source to target*/
    {
        printf("-Matching Descriptors-\n");
        std::cout << " Step 7 of 11:->Matching process.\n";
        {
            printf(" Matching target to source && source to target...\n");
            size_t cGoodS2T=0, cGoodT2S=0;
            float threshold_corr_distance = params.threshold_corr_distance;
            bool hasRepeat = false;
            do {
                cGoodS2T=0; cGoodT2S=0;
                float minDistanceS2T = MAXFLOAT, minDistanceT2S = MAXFLOAT;
                auto MatchDescriptor = [&](PCPtr(DescriptorType) source, PCPtr(DescriptorType) target, pcl::CorrespondencesPtr vSrc2Tar, float& minDis){
                    pcl::KdTreeFLANN<DescriptorType> kdtree;
                    const int k = 1;
                    std::vector<int> k_indices (k);
                    std::vector<float> k_squared_distances (k);
                    kdtree.setInputCloud(target);
                    vSrc2Tar->resize(source->size());
                    float square = threshold_corr_distance*threshold_corr_distance;
                    for (size_t i = 0; i < source->size (); ++i) {
                        kdtree.nearestKSearch (*source, i, k, k_indices, k_squared_distances);
                        if(k_squared_distances[0] < minDis) minDis = k_squared_distances[0];
                        if(k_squared_distances[0] < square){
                            vSrc2Tar->at(i).index_query = i;
                            vSrc2Tar->at(i).index_match = k_indices[0];
                            vSrc2Tar->at(i).distance = k_squared_distances[0];
                        } else {
                            vSrc2Tar->at(i).index_query = i;
                            vSrc2Tar->at(i).index_match = -1;
                            vSrc2Tar->at(i).distance = -1;
                        }
                    }
                };
                MatchDescriptor(mPFHSourceDescriptors, mPFHTargetDescriptors, mCorrespondences_Source2target, std::ref(minDistanceS2T));
                MatchDescriptor(mPFHTargetDescriptors, mPFHSourceDescriptors, mCorrespondences_Target2source, std::ref(minDistanceT2S));
                for (auto& cc : *mCorrespondences_Source2target) if(cc.index_match>0) cGoodS2T++;
                for (auto& cc : *mCorrespondences_Target2source) if(cc.index_match>0) cGoodT2S++;

                printf("  Good mCorrespondences_Source2target: %zu\n", cGoodS2T);
                printf("  Good mCorrespondences_Target2source: %zu\n", cGoodT2S);
                if(!cGoodS2T || !cGoodT2S) {
                    if(hasRepeat){
                        printf("Cannot find any pairs. The descriptor/ keypoint/ correspondence threshold are bad.\n");
                        exit(-1);
                    }
                    auto min = std::min(minDistanceS2T, minDistanceT2S);
                    if(!cGoodT2S) printf("  Cannot find any pairs in Target to Source\n");
                    if(!cGoodS2T) printf("  Cannot find any pairs in Source to Target\n");
                    printf("   Change thres from (%f) to 2 times(%f)\n", threshold_corr_distance, min);
                    threshold_corr_distance = 2*min;
                    hasRepeat = true;
                }
            } while (!cGoodS2T || !cGoodT2S);

            printf(" Done\n");
        }
        
        {
            /*Matching Final Filter: get the final correspondences*/
            mFinalCorrespondences->clear();
            mFinalCorrespondences->reserve(mCorrespondences_Source2target->size ());
            printf(" Filter by checking the above two are matched...\n");
            for (auto cIdx = 0; cIdx < mCorrespondences_Source2target->size (); ++cIdx){
                if(mCorrespondences_Source2target->at(cIdx).index_match==-1) continue;
                if(mCorrespondences_Target2source->at(mCorrespondences_Source2target->at(cIdx).index_match).index_match == -1) continue;
                if (mCorrespondences_Target2source->at(mCorrespondences_Source2target->at(cIdx).index_match).index_match == static_cast<int> (cIdx)){
                    mFinalCorrespondences->push_back(mCorrespondences_Source2target->at(cIdx));
                }
            }
            
            mFinalCorrespondences->resize (mFinalCorrespondences->size());
//            for (unsigned cIdx = 0; cIdx < correspondences->size(); ++cIdx) {
//                (*mFinalCorrespondences)[cIdx].index_query = correspondences[cIdx].first;
//                (*mFinalCorrespondences)[cIdx].index_match = correspondences[cIdx].second;
//            }

            printf("  mFinalCorrespondences->size(): %zu\n", mFinalCorrespondences->size());
            printf(" Done\n");
        }
        if(1)
        {
            printf("  Filter by using CorrespondenceRejectorSampleConsensus\n");
            pcl::registration::CorrespondenceRejectorSampleConsensus<PointT> rejector;
            rejector.setInputSource(mSourceKeypoints);
            rejector.setInputTarget(mTargetKeypoints);
            rejector.setInputCorrespondences(mFinalCorrespondences);
            float outlierThres = params.CPSCInlierThreshold;
            do {
                rejector.setInlierThreshold(outlierThres);
                rejector.getCorrespondences(*mFinalCorrespondences);
                outlierThres *= 1.5;
            } while (!mFinalCorrespondences->size());
            printf(" Done\n");
            std::cout << "  Step 9 of 11:->Final Correspondences process finished: " << mFinalCorrespondences->size() << std::endl;
            std::cout << "  Final threshold: " << outlierThres/1.5 << std::endl;
        }
#ifdef USE_VIEWER
        printf("  Show correspondences. \n");
        viewer->removeAllPointClouds();
        viewer->createViewPort(0, 0, 1, 1, vp1);
        viewer->addPointCloud(mSourceCloud, "cloud_src");
        viewer->addPointCloud(mTargetCloud, "cloud_trg");
        viewer->addPointCloud(mSourceKeypoints, "mSrcKey");
        viewer->addPointCloud(mTargetKeypoints, "mTarKey");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, "cloud_src");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 0, 1, "cloud_trg");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0.9, 0.2, 0, "mSrcKey");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0.9, 0, 0.2, "mTarKey");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "mSrcKey");
        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "mTarKey");
        viewer->addCorrespondences<PointT>(mSourceKeypoints, mTargetKeypoints, *mFinalCorrespondences);
//        for(int c=0;c<mFinalCorrespondences->size(); ++c)
//            viewer->addLine(mSourceKeypoints->at(mFinalCorrespondences->at(c).index_query), mTargetKeypoints->at(mFinalCorrespondences->at(c).index_match), 1, 0, 0, std::to_string(c));
        viewer->spin();
#endif
        printf("-Done!.\n");
        printf("---\n");
    }
    
    /* Initial transformation*/
    {
        printf("-Initial transformation-\n");
        std::cout << " Step 10 of 11:->Initial transformation computed\n" << std::endl;
        pcl::registration::TransformationEstimation<PointT, PointT>::Ptr transformation2 (new pcl::registration::TransformationEstimationSVD<PointT, PointT>);
        transformation2->estimateRigidTransformation (*mSourceKeypoints, *mTargetKeypoints, *mFinalCorrespondences, mInitialTransformationMatrix);
//        transformation2->estimateRigidTransformation (*mTargetKeypoints, *mSourceKeypoints, *mFinalCorrespondences, mInitialTransformationMatrix);
        pcl::transformPointCloud(*mSourceCloud, *mSourceTransformedCloud, mInitialTransformationMatrix);
        printf(" Done!.\n");
        printf(" Show initial transform \n");
        
#ifdef USE_VIEWER
        viewer->removeAllPointClouds();
        viewer->removeAllShapes();
        viewer->addPointCloud(mSourceTransformedCloud, "tSrc", vp1);
        viewer->addPointCloud(mTargetCloud, "Tar", vp1);
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 1, 1, 0, "tSrc");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, "Tar");
        viewer->spin();
        viewer->removePointCloud("tSrc");
        viewer->removePointCloud("Tar");
#endif
        printf("-Done!.\n");
        printf("---\n");
    }
    
    /* Refinement using ICP */
#if 1
    {
        printf("-Transformation refinement using ICP-\n");
        pcl::IterativeClosestPointWithNormals<pcl::PointNormal, pcl::PointNormal> icp;
        PCPtr(pcl::PointNormal) mSourcePointNormal (new PC(pcl::PointNormal));
        PCPtr(pcl::PointNormal) mTargetPointNormal (new PC(pcl::PointNormal));
        pcl::concatenateFields (*mSourceCloud, *mSourceNormals, *mSourcePointNormal);
        pcl::concatenateFields (*mTargetCloud, *mTargetNormals, *mTargetPointNormal);
        pcl::transformPointCloud(*mSourcePointNormal, *mSourcePointNormal, mInitialTransformationMatrix);
        
        icp.setInputSource(mSourcePointNormal);
        icp.setInputTarget(mTargetPointNormal);
        
        params.initICP(maxRad);
        icp.setMaximumIterations(params.ICP.maxIter);
        icp.setMaxCorrespondenceDistance (params.ICP.MaxCorrespondenceDistance);
        icp.setRANSACOutlierRejectionThreshold(params.ICP.RANSACOutlierRejectionThreshold);
        icp.setEuclideanFitnessEpsilon(params.ICP.delta_euclidean);
        icp.setTransformationEpsilon(params.ICP.delta);
        PCPtr(pcl::PointNormal) mSourceTransformedCloudNormal (new PC(pcl::PointNormal));
        icp.align(*mSourceTransformedCloudNormal);
        
        std::cout << "  has converged:" << icp.hasConverged() << " score: " <<
        icp.getFitnessScore() << std::endl;
        std::cout << icp.getFinalTransformation() << std::endl;
        
        mInitialTransformationMatrix = icp.getFinalTransformation() * mInitialTransformationMatrix;
        
        printf("-Done!.\n");
        printf("---\n");
    }
#else
    /*Final Transformation. Utilizamos la libreria libICP*/
    if(0)
    {
        printf("-Transformation refinement using ICP-\n");
        //La matriz de rotación es de 3x3 y la de traslación de 3x1
        Matrix R(3,3);
        Matrix t(3,1);
        
        //Rellenamos la matriz de rotación
        R.val[0][0]=mInitialTransformationMatrix(0,0);
        R.val[0][1]=mInitialTransformationMatrix(0,1);
        R.val[0][2]=mInitialTransformationMatrix(0,2);
        R.val[1][0]=mInitialTransformationMatrix(1,0);
        R.val[1][1]=mInitialTransformationMatrix(1,1);
        R.val[1][2]=mInitialTransformationMatrix(1,2);
        R.val[2][0]=mInitialTransformationMatrix(2,0);
        R.val[2][1]=mInitialTransformationMatrix(2,1);
        R.val[2][2]=mInitialTransformationMatrix(2,2);
        
        //Rellenamos la matriz de traslación
        t.val[0][0]=mInitialTransformationMatrix(0,3);
        t.val[1][0]=mInitialTransformationMatrix(1,3);
        t.val[2][0]=mInitialTransformationMatrix(2,3);
        
        //Mediante memoria dinámica. Creamos uno para sourceCloud y otro para targetKeypoints.
        double* sourceICP = (double*)calloc(3*mSourceCloud->size(),sizeof(double));
        double* targetICP = (double*)calloc(3*mTargetCloud->size(),sizeof(double));
        
        //Relleno sourceIcp con los datos correspondientes
        for(int i=0; i<mSourceCloud->size(); i++) {
            sourceICP[i*3+0] = mSourceCloud->points[i].x;
            sourceICP[i*3+1] = mSourceCloud->points[i].y;
            sourceICP[i*3+2] = mSourceCloud->points[i].z;
        }
        
        //Lo Mismo para targetICP
        for(int i=0; i<mTargetCloud->size(); i++) {
            targetICP[i*3+0] = mTargetCloud->points[i].x;
            targetICP[i*3+1] = mTargetCloud->points[i].y;
            targetICP[i*3+2] = mTargetCloud->points[i].z;
        }
        
        IcpPointToPlane objetoICP(targetICP, mTargetCloud->size(), 3); //3=numero de coordenadas = X-Y-Z
        
        params.initICP(maxRad);
        objetoICP.setMaxIterations(params.ICP.maxIter);
        objetoICP.setMinDeltaParam(params.ICP.delta);
        objetoICP.fit(sourceICP,mSourceCloud->size(),R,t, -1 ); //-1 indicates the "inlier points". Being -1 means that all points are used
        
        mInitialTransformationMatrix(0,0)=R.val[0][0];
        mInitialTransformationMatrix(0,1)=R.val[0][1];
        mInitialTransformationMatrix(0,2)=R.val[0][2];
        mInitialTransformationMatrix(0,3)=t.val[0][0];
        
        mInitialTransformationMatrix(1,0)=R.val[1][0];
        mInitialTransformationMatrix(1,1)=R.val[1][1];
        mInitialTransformationMatrix(1,2)=R.val[1][2];
        mInitialTransformationMatrix(1,3)=t.val[1][0];
        
        mInitialTransformationMatrix(2,0)=R.val[2][0];
        mInitialTransformationMatrix(2,1)=R.val[2][1];
        mInitialTransformationMatrix(2,2)=R.val[2][2];
        mInitialTransformationMatrix(2,3)=t.val[2][0];
        
        //Free Memory
        free(sourceICP);
        free(targetICP);
    }
#endif
    
    {
        printf("-Final Result-\n");
        std::cout << " Final transformation Matrix is : \n";
        std::cout << mInitialTransformationMatrix << std::endl;

        //apply the transformation using PCL now
        pcl::transformPointCloud(*mSourceCloudCopy, *mSourceRegisteredCloud, mInitialTransformationMatrix);
        
#ifdef USE_VIEWER
        viewer->addPointCloud(mSourceRegisteredCloud, "ICPCloud", vp1);
        viewer->addPointCloud(mTargetCloudCopy, "Tar", vp1);
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 1, 1, 0, "ICPCloud");
        viewer->setPointCloudRenderingProperties(visualization::PCL_VISUALIZER_COLOR, 0, 1, 0, "Tar");
        viewer->spin();
        viewer->removePointCloud("ICPCloud");
        viewer->removePointCloud("Tar");
#endif
        
        /*We keep the cloud aligned*/
        std::string routeRegistered = rutaSalida + "_registered.ply";
        std::string routeMerged = rutaSalida + "_merged.ply";
        pcl::io::savePLYFile(routeRegistered, *mSourceRegisteredCloud);
        
        PCPointTPtr merged_cloud (new PCPointT);
        for(auto point: mSourceRegisteredCloud->points){
            merged_cloud->push_back(point);
        }
        for(auto point: mTargetCloudCopy->points){
            merged_cloud->push_back(point);
        }
        pcl::io::savePLYFile(routeMerged, *merged_cloud);
        std::cout << "\n\nStep 11 of 11:->Process finished.\n" << std::endl;
    }
    return 0;
}

