//
//  Header.h
//  main
//
//  Created by Shun-Cheng Wu on 05/06/2018.
//

#ifndef Header_h
#define Header_h

using namespace pcl;
typedef PointXYZ PointT;
typedef PointXYZI PointTI;
typedef PointXYZINormal PointTIN;
typedef PointCloud<PointT> PCPointT;
typedef PCPointT::Ptr PCPointTPtr;
typedef pcl::Normal PointN;
typedef PointCloud<PointN> PCPointN;
typedef PointCloud<PointN>::Ptr PCPointNPtr;
typedef pcl::PFHSignature125 DescriptorType;
#define PCPtr(x) pcl::PointCloud<x>::Ptr
#define PC(x) pcl::PointCloud<x>

using std::string;
using std::vector;

int xyzrgbloader(string pth, PCPointTPtr pc, char sepstring = '\t'){
    pc->clear();
    std::ifstream file;
    file.open(pth);
    assert(file.is_open());
    string line;
    //process line
    while(!file.eof()){
        // get one line
        std::getline(file, line);
        if(line == "") continue;
        // process line
        std::istringstream iss(line);
        std::string token;
        vector<float> tokens;
        while(std::getline(iss, token, sepstring)){
            tokens.push_back(std::stof(token));
        }
        assert(tokens.size() == 6);
        PointT point;
        point.x = tokens[0];
        point.y = tokens[1];
        point.z = tokens[2];
//        point.r = tokens[3];
//        point.g = tokens[4];
//        point.b = tokens[5];
        pc->push_back( point );
    }
    return 1;
}

/*Function to extract calculated keypoints from cloud
 It is called after harris 3d keypoints is computed*/
void getKeypointsCloud(const PCPointTPtr& cloud, const pcl::PointCloud<PointTI>::Ptr& keypoints, PCPointTPtr& cloud_keypoints) {
    // Sanity check
    if (!cloud || !keypoints || cloud->points.empty() || keypoints->points.empty()) {
        std::cout << "getKeyPointsCloud() -> Fail: Something is empty...." << std::endl;
        std::cout << "Cloud size: "  << cloud->size() << std::endl;
        std::cout << "keyPoints size: "  << keypoints->size() << std::endl;
        return;
    }
    
    pcl::KdTreeFLANN<PointT> kdtree;
    kdtree.setInputCloud(cloud);
    
    for (size_t i=0; i<keypoints->size(); ++i) {
        // Get the point in the pointcloud
        PointTI pt_tmp = keypoints->points[i];
        PointT pt;
        pt.x = pt_tmp.x;
        pt.y = pt_tmp.y;
        pt.z = pt_tmp.z;
        
        if (!pcl_isfinite(pt.x) || !pcl_isfinite(pt.y) || !pcl_isfinite(pt.z))
            continue;
        
        // Search this point into the cloud
        std::vector<int> idx_vec;
        std::vector<float> dist;
        if (kdtree.nearestKSearch(pt, 1, idx_vec, dist) > 0)
        {
            //std::cout<< "Dist: "<<dist[0] << std::endl;
            if (dist[0] < 0.0001)
                cloud_keypoints->points.push_back(cloud->points[idx_vec[0]]);
        }
    }
    std::cout<< "Total points: "<<cloud_keypoints->size() << std::endl;
    
}

/*Fucntion to compute the average minimum distance between points in cloud*/
void calcularDistanciaMinimaMedia(PCPointTPtr mCloud, float& distReturned) {
    pcl::search::KdTree<PointT>::Ptr kdtree (new pcl::search::KdTree<PointT> ());
    float totalDistance=0;
    float meanDistance=0;
    //    float distReturned = 0;
    
    int totalcount = mCloud->size() ;
    kdtree->setInputCloud (mCloud);
    int K = 2; //We search 2 pointa because point corresponding to k=1 would be the own point.
    
    std::vector<int> pointIdxNKNSearch(K);
    std::vector<float> pointNKNSquaredDistance(K);
    
    for (int i = 0; i < totalcount; i++) {
        pointIdxNKNSearch.clear();
        pointNKNSquaredDistance.clear();
        if(kdtree->nearestKSearch (mCloud->points[i], K, pointIdxNKNSearch, pointNKNSquaredDistance)>0){
            if(std::sqrt(pointNKNSquaredDistance[1])>0){
                totalDistance = totalDistance + std::sqrt(pointNKNSquaredDistance[1]);
            }
        }
    }
    
    //calculating the mean distance
    meanDistance = totalDistance/totalcount;
    std::cout << "Average Minimum distance = " << meanDistance << std::endl;
    
    //Radius search is set to 3 times the meanDistance
    distReturned = meanDistance * 3;
}

/*Function to delete points with nan normals after normal computation.*/
void removeNANelements(PCPointTPtr mCloud, PCPointNPtr mNormals) {
    pcl::PointCloud<PointTIN>::Ptr cloudINormals (new pcl::PointCloud<PointTIN>());
    std::vector<int> indices;
    
    //Create a Pointcloud with x-y-z-nx-ny-nz
    for (int i = 0; i<mCloud->size(); i++){
        PointTIN p;
        p.intensity = 0;
        p.x = mCloud->points[i].x;
        p.y = mCloud->points[i].y;
        p.z = mCloud->points[i].z;
        p.normal_x = mNormals->points[i].normal_x;
        p.normal_y = mNormals->points[i].normal_y;
        p.normal_z = mNormals->points[i].normal_z;
        cloudINormals->push_back(p);
    }
    //Delete all points with wrong normals
    pcl::removeNaNNormalsFromPointCloud(*cloudINormals, *cloudINormals, indices);
    
    //Reconstruct pointclouds with resultant points
    mCloud->clear();
    mNormals->clear();
    
    for (int i = 0; i<cloudINormals->size(); i++){
        PointT p;
        PointN n;
        p.x = cloudINormals->points[i].x;
        p.y = cloudINormals->points[i].y;
        p.z = cloudINormals->points[i].z;
        
        n.normal_x = cloudINormals->points[i].normal_x;
        n.normal_y = cloudINormals->points[i].normal_y;
        n.normal_z = cloudINormals->points[i].normal_z;
        
        mCloud->push_back(p);
        mNormals->push_back(n);
    }
}

#endif /* Header_h */
