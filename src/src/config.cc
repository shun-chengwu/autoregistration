#include "config.h"

Parameters::Parameters(){
    downsampleRad = 2.5;
    downsampleRad = 1.5;
    downsampleRad = 0.01;
    threshold_corr_distance = 10;
//    threshold_corr_distance = 3;
    CPSCInlierThreshold = 0.01;
    
    StatisticalOutlierRemoval.std = 10;
    StatisticalOutlierRemoval.meank = 1.5;
}

void Parameters::initISS(float rad){
    ISS.issBaseRad = rad*0.3;
    ISS.salientRad = 6 * ISS.issBaseRad;
    ISS.nonMaxRad = 2 * ISS.issBaseRad;
    ISS.normalRad = 2 * ISS.issBaseRad;
    ISS.borderRad = 1 * ISS.issBaseRad;
    ISS.thres21 = 0.975;
    ISS.thres32 = 0.975;
    ISS.minNeighbors = 5;
    ISS.numThread = 4;
    
//    ISS.thres21 = 0.975 * 0.5;
//    ISS.thres32 = 0.975 * 0.5;
//    ISS.nonMaxRad = 2 * ISS.issBaseRad;
//    ISS.normalRad = 2 * ISS.issBaseRad;
//    ISS.salientRad = 9 * ISS.issBaseRad;
//    ISS.borderRad = 0.5 * ISS.issBaseRad;
}
void Parameters::initDesp(float rad){
    Desp.rad = rad*30;
}
void Parameters::initICP(float rad){
    ICP.MaxCorrespondenceDistance = rad;
    ICP.RANSACOutlierRejectionThreshold = rad * 0.1;
    ICP.delta_euclidean = rad * 1e-5;
    ICP.maxIter = 1000;
    ICP.delta = 1e-7;
    
    ICP.MaxCorrespondenceDistance = rad * 3;
    ICP.RANSACOutlierRejectionThreshold = rad * 0.5;
}
