//
//  Header.h
//  main
//
//  Created by Shun-Cheng Wu on 05/06/2018.
//

#ifndef Config_h
#define Config_h

#include <stdlib.h>

class Parameters{
public:
    Parameters();

    struct {
        float issBaseRad;
        float salientRad;
        float nonMaxRad;
        float normalRad;
        float borderRad;
        float thres21;
        float thres32;
        int minNeighbors;
        int numThread;
    } ISS;
    void initISS(float rad);
    
    struct {
        float rad;
    } Desp;
    void initDesp(float rad);
    
    struct {
        float MaxCorrespondenceDistance;
        float RANSACOutlierRejectionThreshold;
        float maxIter;
        float delta;
        float delta_euclidean;
    } ICP;
    void initICP(float rad);
    
    struct {
        float std;
        int meank;
    } StatisticalOutlierRemoval;
    
    float downsampleRad;
    float threshold_corr_distance;
    /** CorrespondenceRejectorSampleConsensus */
    float CPSCInlierThreshold;
};

#endif /* Config_h */
